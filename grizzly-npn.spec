Name:                grizzly-npn
Version:             1.8.1
Release:             2
Summary:             Grizzly Next Protocol Negotiation API
License:             CDDL-1.1 or GPLv2 with exceptions
URL:                 https://grizzly.java.net/spdy.html
Source0:             https://github.com/javaee/%{name}/archive/1_8_1/%{name}-1_8_1.tar.gz
Source1:             https://javaee.github.io/glassfish/LICENSE
BuildRequires:       maven-local mvn(net.java:jvnet-parent:pom:)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:       mvn(org.osgi:org.osgi.compendium) mvn(org.osgi:org.osgi.core)
BuildArch:           noarch
%description
A pure Java implementation of the
Next Protocol Negotiation TLS Extension
for OpenJDK 7 or greater.
NPN allows the application layer to
negotiate which protocol to use over the
secure connection.

%package bootstrap
Summary:             Grizzly NPN Bootstrap
%description bootstrap
This package contains the JAR that
will be placed on the bootclasspath
in order for NPN to work.

%package osgi
Summary:             Grizzly NPN OSGi
%description osgi
This empty module allows the bootclasspath classes in
org.glassfish.grizzly.npn to be available via the
OSGi classloading mechanisms.
Using GlassFish as an example:
- grizzly-npn-bootstrap.jar goes into the
  domain's bootclasspath (-Xbootclasspath/p:[PATH TO THE JAR])
- grizzly-npn-osgi and grizzly-spdy JARs go into the
  [PATH TO THE GlassFish 4 HOME]/modules directory.


%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n %{name}-1_8_1
cp -p %{SOURCE1} LICENSE.html
sed -i "s,59 Temple Place,51 Franklin Street,;s,Suite 330,Fifth Floor,;s,02111-1307,02110-1301,"  $(find -name "*.java") LICENSE.html
mkdir -p osgi/target/classes/META-INF
touch osgi/target/classes/META-INF/MANIFEST.MF
rm -r bootstrap/src/main/java/sun/security/ssl/Alerts.java \
 bootstrap/src/main/java/sun/security/ssl/ClientHandshaker.java \
 bootstrap/src/main/java/sun/security/ssl/ExtensionType.java \
 bootstrap/src/main/java/sun/security/ssl/Handshaker.java \
 bootstrap/src/main/java/sun/security/ssl/HandshakeMessage.java \
 bootstrap/src/main/java/sun/security/ssl/HelloExtensions.java \
 bootstrap/src/main/java/sun/security/ssl/ServerHandshaker.java \
 bootstrap/src/main/java/sun/security/ssl/SSLEngineImpl.java \
 bootstrap/src/main/java/sun/security/ssl/AlpnExtension.java \
 bootstrap/src/main/java/sun/security/ssl/NextProtocolNegotiationExtension.java

%pom_xpath_remove "pom:plugin[pom:artifactId = 'maven-bundle-plugin']/pom:configuration/pom:instructions/pom:Export-Package" bootstrap
%pom_xpath_inject "pom:plugin[pom:artifactId = 'maven-bundle-plugin']/pom:configuration/pom:instructions" "
<Export-Package>
    sun.security.ssl
</Export-Package>
<Import-Package>
    org.glassfish.grizzly.npn
</Import-Package>" bootstrap
%mvn_package :%{name} %{name}
%mvn_package :%{name}-api %{name}

%build
%mvn_build -s

%install
%mvn_install

%files -f .mfiles-%{name}
%license LICENSE.html

%files bootstrap -f .mfiles-%{name}-bootstrap
%license LICENSE.html

%files osgi -f .mfiles-%{name}-osgi
%license LICENSE.html

%files javadoc -f .mfiles-javadoc
%license LICENSE.html

%changelog
* Fri Jul 19 2024 wulei <wu_lei@hoperun.com> - 1.8.1-2
- Regulate license information

* Mon Jan 29 2024 Ge Wang <wang__ge@126.com> - 1.8.1-1
- Update to version 1.8.1

* Mon Nov 9 2020 wutao <wutao61@huawei.com> - 1.2-2
- fix build error

* Sat Aug 15 2020 Ge Wang <wangge20@huawei.com> - 1.2-1
- Package init
